﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TypeUtils
{
    public static class TypeMemberExtensions
    {

        public static List<TypeMember> GetWithAttribute(this Type type, params Type[] attributeTypes)
        {
            // Create empty list of type values.
            var typeMembers = new List<TypeMember>();

            // Loop over all fields and create type values for each of them having the given attributes.
            List<FieldInfo> fieldInfos = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(fi => fi.GetCustomAttributes().Count(ca => attributeTypes.Contains(ca.GetType())) > 0).ToList();

            foreach (var fieldInfo in fieldInfos)
            {
                foreach (var attributeType in attributeTypes)
                {
                    var attributes = fieldInfo.GetCustomAttributes(attributeType).ToArray();
                    if (attributes.Any())
                    {
                        // Create getter and setter functions.
                        object Getter(object obj) => fieldInfo.GetValue(obj);
                        void Setter(object obj, object value) => fieldInfo.SetValue(obj, value);
                        // All new type value object to the list.
                        typeMembers.Add(new TypeMember(fieldInfo.Name, Getter, Setter, attributes, MemberCategory.Field, fieldInfo.FieldType, fieldInfo.IsPublic, fieldInfo.IsPublic));
                    }
                }
            }

            // Loop over all properties and create type values for each of them having the given attributes.
            List<PropertyInfo> propInfos = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(pi => pi.GetCustomAttributes().Count(ca => attributeTypes.Contains(ca.GetType())) > 0).ToList();

            foreach (var propInfo in propInfos)
            {
                foreach (var attributeType in attributeTypes)
                {
                    var attributes = propInfo.GetCustomAttributes(attributeType).ToArray();
                    if (attributes.Any())
                    {
                        // Create getter and setter functions.
                        object Getter(object obj) => propInfo.GetValue(obj);
                        void Setter(object obj, object value) => propInfo.SetValue(obj, value);
                        // All new type value object to the list.
                        typeMembers.Add(new TypeMember(propInfo.Name, Getter, Setter, attributes, MemberCategory.Field, propInfo.PropertyType, propInfo.CanRead, propInfo.CanWrite));
                    }
                }
            }

            // Return the list.
            return typeMembers;
        }

    }
}
