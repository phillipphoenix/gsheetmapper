﻿using System;
using System.Collections.Generic;

namespace TypeUtils
{
    public class TypeMember
    {
        public string MemberName { get; private set; }
        public bool IsReadPublic { get; private set; }
        public bool IsWritePublic { get; private set; }
        public MemberCategory MemberCategory { get; private set; }
        public Type MemberType { get; private set; }

        public Attribute[] Attributes { get; private set; }

        private readonly Func<object, object> _getter;
        private readonly Action<object, object> _setter;

        public TypeMember(string memberName, Func<object, object> getter, Action<object, object> setter, Attribute[] attributes, MemberCategory memberCategory, Type memberType, bool isReadPublic, bool isWritePublic)
        {
            MemberName = memberName;
            _getter = getter;
            _setter = setter;
            Attributes = attributes;
            MemberCategory = memberCategory;
            MemberType = memberType;
            IsReadPublic = isReadPublic;
            IsWritePublic = isWritePublic;
        }

        public object Get(object obj)
        {
            return _getter(obj);
        }

        public void Set(object obj, object value)
        {
            _setter(obj, value);
        }
    }

    public enum MemberCategory
    {
        Field, Property
    }
}
