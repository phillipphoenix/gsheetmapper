# GSheetMapper

A small C# library to make easily map from rows in a google sheet to a model (or class) in C#.

## How to use ##

### Example ###

An example showing how to loop over and create all entities from a sheet.

```csharp

// The spread sheet ID can be found in the URL after having navigated to the spread sheet in the browser.
var spreadSheet = SpreadSheetFactory.GetSpreadSheet("1ZS_Rw32TpW4Xh91gIoQYFwXpiWOGjAfT2-aYp8aCFEA");
// The GetTable method takes the type of model/class that the table/sheet with the given name maps to.
// Remember that the sheet name must be the exact same as in your spread sheet.
var table = spreadSheet.GetTable<TestModel>("Test Sheet");

// Loop over the entities and do stuff with your entities!
for (int i = 0; i < table.RowCount; i++) {
	var entity = table.GetEntityByIndex(i);
	// DO SOMETHING WITH YOUR ENTITY!
}

```