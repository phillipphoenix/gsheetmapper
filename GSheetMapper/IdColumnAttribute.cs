﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class IdColumnAttribute : Attribute, IColumnAttribute
    {

        public string HeaderName { get; private set; }
        public IdType IdType { get; private set; }

        public IdColumnAttribute(string headerName, IdType idType)
        {
            HeaderName = headerName;
            IdType = idType;
        }
    }

    public enum IdType
    {
        String, Integer
    }
}
