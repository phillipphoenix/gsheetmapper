﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class CollectionColumnAttribute : Attribute, IColumnAttribute
    {

        public string[] HeaderNames { get; private set; }
        public CollectionType CollectionType { get; private set; }
        public ColumnType ColumnType { get; private set; }

        public CollectionColumnAttribute(string[] headerNames, CollectionType collectionType, ColumnType columnType)
        {
            HeaderNames = headerNames;
            CollectionType = collectionType;
            ColumnType = columnType;
        }
    }

    public enum CollectionType
    {
        None, Array, List, Dictionary
    }
}
