﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ColumnAttribute : Attribute, IColumnAttribute
    {

        public string HeaderName { get; private set; }
        public ColumnType ColumnType { get; private set; }

        public ColumnAttribute(string headerName, ColumnType columnType)
        {
            HeaderName = headerName;
            ColumnType = columnType;
        }
    }
}
