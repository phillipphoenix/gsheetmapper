﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    public interface IWorkSheet
    {

        string Name { get; }
        Dictionary<string, int> Headers { get; }

    }
}
