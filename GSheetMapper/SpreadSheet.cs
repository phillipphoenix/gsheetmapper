﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;

namespace GSheetMapper
{
    public class SpreadSheet
    {
        public string Id { get; private set; }

        private readonly SheetsService _sheetService;
        private readonly Dictionary<string, IWorkSheet> _tables = new Dictionary<string, IWorkSheet>();

        internal SpreadSheet(string spreadSheetId, SheetsService sheetService)
        {
            Id = spreadSheetId;
            _sheetService = sheetService;
        }

        /// <summary>
        /// Gets a worksheet object of type T from the worksheet with the given name.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="workSheetName"></param>
        /// <param name="instantiatorFunc">A function to instantiate an object of type T. Activator.CreateInstance will be used if null.</param>
        /// <param name="forceRefresh">If true, don't use cached worksheet.</param>
        /// <returns></returns>
        public WorkSheet<T> GetWorkSheet<T>(string workSheetName, Func<T> instantiatorFunc = null, bool forceRefresh = false)
        {
            // If we already converted the data from the workSheet, fetch that.
            if (!forceRefresh && _tables.ContainsKey(workSheetName) && _tables[workSheetName].GetType() == typeof(WorkSheet<T>))
            {
                return (WorkSheet<T>)_tables[workSheetName];
            }

            // To get all cells in first row of a sheet, specify cell range as just '1:1' :
            // https://stackoverflow.com/a/45550453
            //sheetService.Spreadsheets.Values.Get("SpreadSheetID", "SheetID!1:1");

            // To get all cells in a sheet, don't specify cell range, just use sheet ID as range:
            // https://stackoverflow.com/a/45550453
            //sheetService.Spreadsheets.Values.Get("SpreadSheetID", "SheetID");

            // Get headers and data.
            SpreadsheetsResource.ValuesResource.GetRequest tableRequest = _sheetService.Spreadsheets.Values.Get(Id, workSheetName);
            ValueRange tableResponse = tableRequest.Execute();

            IList<IList<object>> tableValues = tableResponse.Values;
            if (tableValues == null || tableValues.Count == 0)
            {
                throw new Exception($"No values found in workSheet with the name {workSheetName}.");
            }

            // Select headers as string array.
            var headers = tableValues[0].Select(headerValue => headerValue.ToString()).ToArray();
            var data = tableValues.Skip(1).Select(rowList => rowList.ToArray()).ToList();

            // Create workSheet.
            var table = new WorkSheet<T>(headers, data, instantiatorFunc);

            return table;
        }

    }
}
