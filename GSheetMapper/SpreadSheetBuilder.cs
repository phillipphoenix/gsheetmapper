﻿using System;
using System.Collections.Generic;
using System.IO;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;

namespace GSheetMapper
{
    public class SpreadSheetBuilder
    {
        // TODO: Check if this can be changed or if I need to change it online as well.
        private const string ApplicationName = "Google Sheets Importer";
        
        private string _spreadSheetId;

        public SpreadSheetBuilder AuthenticateWithFile(string clientIdFileName = "client_id.json")
        {
            if (!Authenticator.IsAuthenticated)
            {
                Authenticator.AuthenticateWithFilePath(clientIdFileName);
            }
            return this;
        }

        public SpreadSheetBuilder AuthenticateWithContent(string clientIdContent)
        {
            if (!Authenticator.IsAuthenticated)
            {
                Authenticator.AuthenticateWithContent(clientIdContent);
            }
            return this;
        }

        public SpreadSheetBuilder AuthenticateWithStream(Stream clientIdStream)
        {
            if (!Authenticator.IsAuthenticated)
            {
                Authenticator.Authenticate(clientIdStream);
            }
            return this;
        }

        public SpreadSheetBuilder SetSpreadSheetId(string spreadSheetId)
        {
            _spreadSheetId = spreadSheetId;
            return this;
        }

        public SpreadSheet Build()
        {
            if (!Authenticator.IsAuthenticated)
            {
                throw new Exception("Client not authenticated. Either all authentication methods added failed or none were used.");
            }

            // Create Google Sheets API service.
            var sheetService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Authenticator.UserCredential,
                ApplicationName = ApplicationName,
            });

            var spreadSheet = new SpreadSheet(_spreadSheetId, sheetService);

            return spreadSheet;
        }
    }
}