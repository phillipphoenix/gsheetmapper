﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    public class WorkSheetRow
    {

        public IWorkSheet WorkSheet { get; }

        /// <summary>
        /// Get value in row from the column with the given name.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public object this[string column]
        {
            get
            {
                if (!WorkSheet.Headers.ContainsKey(column))
                {
                    throw new ArgumentException($"WorkSheet {WorkSheet.Name} doesn't have a column with the name {column}.");
                }
                return _values[WorkSheet.Headers[column]];
            }
        }

        private readonly object[] _values;

        public WorkSheetRow(IWorkSheet workSheet, object[] values)
        {
            WorkSheet = workSheet;
            _values = values;
        }

    }
}
