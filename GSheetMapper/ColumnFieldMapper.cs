﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    /// <summary>
    /// Column Field Mappers help map from a column in the workSheet to a field in an object.
    /// </summary>
    public class ColumnFieldMapper
    {

        public string[] HeaderNames { get; set; }
        public int[] Indexes { get; set; }
        /// <summary>
        /// The type of the column (or columns for collections).
        /// </summary>
        public ColumnType ColumnType { get; set; }
        /// <summary>
        /// The collection type, if mapping from several columns to a single collection field.
        /// </summary>
        public CollectionType CollectionType { get; set; }
        public Type EnumType { get; set; }
        /// <summary>
        /// The two types are the object and the value to set in the object (in that order).
        /// </summary>
        public Action<object, object> SetFieldValue { get; set; }

    }
}
