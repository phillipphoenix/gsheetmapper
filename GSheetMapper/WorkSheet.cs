﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using TypeUtils;

namespace GSheetMapper
{
    /// <summary>
    /// Represents a worksheet in a Google Sheets spreadsheet.
    /// It's assumed that there is no other data in the worksheet than a table with a header row and then rows of data.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WorkSheet<T> : IWorkSheet
    {
        public string Name { get; private set; }
        public Dictionary<string, int> Headers { get; private set; } = new Dictionary<string, int>();
        public List<WorkSheetRow> Rows { get; private set; }

        public int Count => Rows.Count;

        private string _idColumnName;
        private IdType _idColumnIdType;
        private Type _idColumnType;

        private List<ColumnFieldMapper> _columnFieldMappers;

        private readonly Func<WorkSheetRow, T> _entityCreator;

        /// <summary>
        /// Creates a workSheet containing the given headers and an optional ID column name.
        /// </summary>
        /// <param name="headers">String array of headers in order (order indicates column index).</param>
        /// <param name="data">A list containing all the rows of data.</param>
        /// <param name="instantiatorFunc">The function to use to instantiate a new object. If null, the Activator.CreateInstance method is used.</param>
        internal WorkSheet(string[] headers, List<object[]> data, Func<T> instantiatorFunc = null)
        {
            // Add header to column index mapping for all headers.
            for (int i = 0; i < headers.Length; i++)
            {
                Headers.Add(headers[i], i);
            }

            // Add all workSheet rows using the data.
            Rows = data.Select(rowData => new WorkSheetRow(this, rowData)).ToList();

            // Set instantiator function.
            Func<T> instantiator = instantiatorFunc ?? Activator.CreateInstance<T>;

                // Create mappings containing header name, and field setter function etc.
            _columnFieldMappers = CreateColumnFieldMapper(Headers);

            // Define the entity creator function.
            _entityCreator = row =>
            {
                // Create object of type T.
                T obj = instantiator();
                
                foreach (var cfm in _columnFieldMappers)
                {
                    object valueToSet = null;
                    // If we're not dealing with a collection type, assume there's only one header name.
                    if (cfm.CollectionType == CollectionType.None)
                    {
                        var value = row[cfm.HeaderNames[0]];
                        var parsedValue = GetParsedValue(value.ToString(), cfm.ColumnType,
                            cfm.EnumType);
                        // Parse the value and save it as the value to set.
                        valueToSet = parsedValue;
                    }
                    else
                    {
                        object collection;
                        Action<int, string, object> addFunc;
                        switch (cfm.CollectionType)
                        {
                            case CollectionType.Array:
                                {
                                    var type = TypeFromColumnType(cfm.ColumnType, cfm.EnumType);
                                    var array = Array.CreateInstance(type, cfm.Indexes.Length);
                                    collection = array;
                                    addFunc = (index, header, value) => { array.SetValue(value, index); };
                                }
                                break;
                            case CollectionType.List:
                                {
                                    var type = TypeFromColumnType(cfm.ColumnType, cfm.EnumType);
                                    var listType = typeof(List<>).MakeGenericType(type);
                                    collection = Activator.CreateInstance(listType);
                                    var listAddMethod = listType.GetMethod("Add");
                                    addFunc = (index, header, value) => { listAddMethod.Invoke(collection, new[] { value }); };
                                }
                                break;
                            case CollectionType.Dictionary:
                                {
                                    var type = TypeFromColumnType(cfm.ColumnType, cfm.EnumType);
                                    Type dictType = typeof(Dictionary<,>).MakeGenericType(typeof(string), type);
                                    collection = Activator.CreateInstance(dictType);
                                    var dictAddMethod = dictType.GetMethod("Add");
                                    addFunc = (index, header, value) =>
                                    {
                                        dictAddMethod.Invoke(collection, new[] { header, value });
                                    };
                                }
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        for (int x = 0; x < cfm.Indexes.Length; x++)
                        {
                            var curIndex = x;
                            var columnName = cfm.HeaderNames[curIndex];

                            var value = row[columnName];

                            try
                            {
                                object castValue = GetParsedValue(value.ToString(), cfm.ColumnType, cfm.EnumType);
                                addFunc.Invoke(curIndex, columnName, castValue);
                            }
                            catch (ArgumentOutOfRangeException e)
                            {
                                throw new ArgumentException($"Couldn't parse value {value} in column {columnName} to type {cfm.ColumnType}.");
                            }
                        }

                        // After creating a new collection object and adding all values to it, save the collection as the value to set.
                        valueToSet = collection;
                    }

                    // Set object value for current field/property mapping.
                    cfm.SetFieldValue(obj, valueToSet);
                }

                // Return the object.
                return obj;
            };
        }

        public T GetEntityByIndex(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows.Count)
            {
                throw new ArgumentException($"Given row index is out of bounds for workSheet {Name}. Current row count is: {Count}");
            }

            return _entityCreator(Rows[rowIndex]);
        }

        /// <summary>
        /// Returns the entity T with the given ID, if found.
        /// </summary>
        /// <param name="id">The ID of the entity to get. Values are stored as </param>
        /// <returns></returns>
        public T GetEntityById(dynamic id)
        {
            if (id.GetType() != _idColumnType)
            {
                if (_idColumnIdType == null)
                {
                    throw new ArgumentException($"No ID column type was defined. In order to get entity by ID a ID column attribute must be present on the model.");
                }
                throw new ArgumentException($"Id was not of same type as the ID on type {typeof(T).Name} ({_idColumnType.Name}).");
            }

            foreach (var tableRow in Rows)
            {
                if (GetParsedId(tableRow) == id)
                {
                    return _entityCreator(tableRow);
                }
            }

            // Entity with given ID wasn't found, so return default value for type T (null if T is a class).
            return default(T);
        }

        private dynamic GetParsedId(WorkSheetRow row)
        {
            var value = row[_idColumnName];

            switch (_idColumnIdType)
            {
                case IdType.String:
                    return value.ToString();
                case IdType.Integer:
                    if (value is int)
                    {
                        return value;
                    }
                    else
                    {
                        return int.Parse(value.ToString());
                    }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private object GetParsedValue(string value, ColumnType type, Type enumType)
        {
            switch (type)
            {
                case ColumnType.String:
                    return value;
                case ColumnType.Integer:
                    return int.Parse(value);
                case ColumnType.Float:
                    return float.Parse(value);
                case ColumnType.Bool:
                    return bool.Parse(value);
                case ColumnType.Enum:
                    if (enumType == null)
                    {
                        throw new ArgumentException($"No enum type was given for the GetParsedValue method for an enum column type.");
                    }
                    return Enum.Parse(enumType, value, true);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Type TypeFromColumnType(ColumnType columnType, Type enumType)
        {
            switch (columnType)
            {
                case ColumnType.String:
                    return typeof(string);
                case ColumnType.Integer:
                    return typeof(int);
                case ColumnType.Float:
                    return typeof(float);
                case ColumnType.Bool:
                    return typeof(bool);
                case ColumnType.Enum:
                    return enumType;
                default:
                    throw new ArgumentOutOfRangeException(nameof(columnType), columnType, null);
            }
        }

        private ColumnType GetColumnTypeFromIdType(IdType idType)
        {
            switch (idType)
            {
                case IdType.String:
                    return ColumnType.String;
                case IdType.Integer:
                    return ColumnType.Integer;
                default:
                    throw new ArgumentOutOfRangeException(nameof(idType), idType, null);
            }
        }

        private List<ColumnFieldMapper> CreateColumnFieldMapper(Dictionary<string, int> headers)
        {
            // Create a new empty list to keep the field mappers.
            var columnFieldMappers = new List<ColumnFieldMapper>();

            // Get all members of the type with the column attributes attached.
            var typeMembers = typeof(T).GetWithAttribute(typeof(IdColumnAttribute), typeof(ColumnAttribute),
                typeof(CollectionColumnAttribute));

            // Local function that adds a column field mapper from the given data.
            // It is used below.
            void AddMapper(string[] headerNames, int[] indexes, ColumnType columnType, Type enumType, CollectionType collectionType, Action<object, object> setter)
            {
                columnFieldMappers.Add(new ColumnFieldMapper
                {
                    HeaderNames = headerNames,
                    Indexes = indexes,
                    ColumnType = columnType,
                    EnumType = enumType,
                    CollectionType = collectionType,
                    SetFieldValue = setter,
                });
            }

            // Loop over all members with the given attributes.
            foreach (var typeMember in typeMembers)
            {
                // Add mapper for ID columns.
                if (typeMember.Attributes.Any(a => a is IdColumnAttribute))
                {
                    var ica = typeMember.Attributes.First(a => a is IdColumnAttribute) as IdColumnAttribute;
                    if (!headers.ContainsKey(ica.HeaderName))
                    {
                        throw new Exception($"ID column {ica.HeaderName} not found list of loaded headers.");
                    }
                    AddMapper(new []{ica.HeaderName}, new []{headers[ica.HeaderName]}, GetColumnTypeFromIdType(ica.IdType), null, CollectionType.None, typeMember.Set);

                    _idColumnName = ica.HeaderName;
                    _idColumnIdType = ica.IdType;
                    _idColumnType = typeMember.MemberType;
                }
                // Add mapper for columns.
                if (typeMember.Attributes.Any(a => a is ColumnAttribute))
                {
                    var ca = typeMember.Attributes.First(a => a is ColumnAttribute) as ColumnAttribute;
                    if (!headers.ContainsKey(ca.HeaderName))
                    {
                        throw new Exception($"Column {ca.HeaderName} not found list of loaded headers.");
                    }
                    var memberType = ca.ColumnType == ColumnType.Enum ? typeMember.MemberType : null;
                    AddMapper(new[] { ca.HeaderName }, new[] { headers[ca.HeaderName] }, ca.ColumnType, memberType, CollectionType.None, typeMember.Set);
                }
                // Add mapper for collection columns.
                if (typeMember.Attributes.Any(a => a is CollectionColumnAttribute))
                {
                    var cca = typeMember.Attributes.First(a => a is CollectionColumnAttribute) as CollectionColumnAttribute;
                    var notFoundHeaderNames = cca.HeaderNames.Where(hn => !headers.ContainsKey(hn)).ToList();
                    if (notFoundHeaderNames.Count > 0)
                    {
                        throw new Exception($"Collumns {string.Join(", ", notFoundHeaderNames)} from a collection column not found list of loaded headers.");
                    }
                    var memberType = cca.ColumnType == ColumnType.Enum ? typeMember.MemberType : null;
                    AddMapper(cca.HeaderNames, cca.HeaderNames.Select(hn => headers[hn]).ToArray(), cca.ColumnType, memberType, cca.CollectionType, typeMember.Set);
                }
            }

            // Return the mappings.
            return columnFieldMappers;
        }
    }
}
