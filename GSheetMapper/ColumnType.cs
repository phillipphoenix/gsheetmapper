﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSheetMapper
{
    public enum ColumnType
    {
        String, Integer, Float, Bool, Enum
    }
}
