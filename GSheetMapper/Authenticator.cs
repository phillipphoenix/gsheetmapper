﻿using System;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;

namespace GSheetMapper
{
    public static class Authenticator
    {
        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.gsheetmapper.json
        private static readonly string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };
        private const string CredentialsFileName = "sheets.googleapis.gsheetmapper.json";

        public static bool IsAuthenticated => UserCredential != null;
        public static UserCredential UserCredential { get; private set; }

        public static void AuthenticateWithFilePath(string clientIdFilePath = "client_id.json")
        {
            using (var stream = new FileStream(clientIdFilePath, FileMode.Open, FileAccess.Read))
            {
                Authenticate(stream);
            }
        }

        public static void AuthenticateWithContent(string clientIdContent)
        {
            using (var stream = StreamFromString(clientIdContent))
            {
                Authenticate(stream);
            }
        }

        public static void Authenticate(Stream clientIdStream)
        {
            string credPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            credPath = Path.Combine(credPath, $".credentials/{CredentialsFileName}");

            UserCredential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                GoogleClientSecrets.Load(clientIdStream).Secrets,
                Scopes,
                "user",
                CancellationToken.None,
                new FileDataStore(credPath, true)).Result;
            Console.WriteLine("Credential file saved to: " + credPath);
            Console.WriteLine();
        }

        /// <summary>
        /// Signs out the currently signed in user.
        /// </summary>
        public static void SignOut()
        {
            string credPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            credPath = Path.Combine(credPath, $".credentials/{CredentialsFileName}");

            if (Directory.Exists(credPath))
            {
                Directory.Delete(credPath, true);
            }

            UserCredential = null;
        }

        private static Stream StreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

    }
}
