using GSheetMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestProject.Models;

namespace UnitTestProject
{
    [TestClass]
    public class SpreadSheetTests
    {
        [TestMethod]
        public void GetWorkSheetTest()
        {

            var spreadSheet = new SpreadSheetBuilder()
                .AuthenticateWithFile()
                .SetSpreadSheetId("1ZS_Rw32TpW4Xh91gIoQYFwXpiWOGjAfT2-aYp8aCFEA")
                .Build();
            var table = spreadSheet.GetWorkSheet<TestModel>("Test Sheet", null, true);

            Assert.IsNotNull(table);

        }
    }
}
