using System;
using GSheetMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestProject.Models;

namespace UnitTestProject
{
    [TestClass]
    public class WorkSheetTests
    {
        [TestMethod]
        public void GetEntityByIndexTest()
        {

            var spreadSheet = new SpreadSheetBuilder()
                .AuthenticateWithFile()
                .SetSpreadSheetId("1ZS_Rw32TpW4Xh91gIoQYFwXpiWOGjAfT2-aYp8aCFEA")
                .Build();
            var workSheet = spreadSheet.GetWorkSheet<TestModel>("Test Sheet", null, true);

            var entity = workSheet.GetEntityByIndex(0);
            
            Assert.IsNotNull(entity);

            Console.WriteLine("--- Entity ---");
            Console.WriteLine(entity);

            Assert.IsInstanceOfType(entity.Id, typeof(int));
            Assert.IsInstanceOfType(entity.String, typeof(string));
            Assert.IsInstanceOfType(entity.Int, typeof(int));
            Assert.IsInstanceOfType(entity.Float, typeof(float));
            Assert.IsInstanceOfType(entity.Bool, typeof(bool));

        }

        [TestMethod]
        public void GetEntityByIdTest()
        {

            var spreadSheet = new SpreadSheetBuilder()
                .AuthenticateWithFile()
                .SetSpreadSheetId("1ZS_Rw32TpW4Xh91gIoQYFwXpiWOGjAfT2-aYp8aCFEA")
                .Build();
            var workSheet = spreadSheet.GetWorkSheet<TestModel>("Test Sheet", null, true);

            var entity = workSheet.GetEntityById(2);

            Assert.IsNotNull(entity);

            Console.WriteLine("--- Entity ---");
            Console.WriteLine(entity);

            Assert.IsInstanceOfType(entity.Id, typeof(int));
            Assert.IsInstanceOfType(entity.String, typeof(string));
            Assert.IsInstanceOfType(entity.Int, typeof(int));
            Assert.IsInstanceOfType(entity.Float, typeof(float));
            Assert.IsInstanceOfType(entity.Bool, typeof(bool));

        }
    }
}
