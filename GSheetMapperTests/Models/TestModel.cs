﻿using System;
using System.Collections.Generic;
using System.Text;
using GSheetMapper;

namespace UnitTestProject.Models
{
    class TestModel
    {
        [IdColumn("Id", IdType.Integer)]
        public int Id { get; set; }
        [Column("String", ColumnType.String)]
        public string String { get; set; }
        [Column("Int", ColumnType.Integer)]
        public int Int { get; set; }
        [Column("Float", ColumnType.Float)]
        public float Float { get; set; }
        [Column("Bool", ColumnType.Bool)]
        public bool Bool { get; set; }
        [CollectionColumn(new []{"CollectionTest1", "CollectionTest2" , "CollectionTest3" }, CollectionType.Array, ColumnType.String)]
        public string[] Array;
        [CollectionColumn(new[] { "CollectionTest1", "CollectionTest2", "CollectionTest3" }, CollectionType.List, ColumnType.String)]
        public List<string> List;
        [CollectionColumn(new[] { "CollectionTest1", "CollectionTest2", "CollectionTest3" }, CollectionType.Dictionary, ColumnType.String)]
        public Dictionary<string, string> Dictionary;

        public override string ToString()
        {
            string dictionaryString(Dictionary<string, string> dict)
            {
                List<string> strings = new List<string>();
                foreach (var dictKey in dict.Keys)
                {
                    strings.Add($"{{{dictKey} -> {dict[dictKey]}}}");
                }

                return string.Join(", ", strings);
            }

            return $"{Id}: {String}, {Int}, {Float}, {Bool}, [{string.Join(", ", Array)}], [{string.Join(", ", List)}], [{dictionaryString(Dictionary)}]";
        }
    }
}
