using System.IO;
using GSheetMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class AuthenticatorTests
    {
        [TestMethod]
        public void AuthenticateTest()
        {
            Authenticator.AuthenticateWithFilePath();
            Assert.IsTrue(Authenticator.IsAuthenticated);

        }

        [TestMethod]
        public void AuthenticateWithContentTest()
        {
            var clientIdContent = File.ReadAllText("client_id.json");
            Authenticator.AuthenticateWithContent(clientIdContent);
            Assert.IsTrue(Authenticator.IsAuthenticated);
        }

        [TestMethod]
        public void LogoutAuthenticateTest()
        {
            Authenticator.AuthenticateWithFilePath();
            Authenticator.SignOut();
            Assert.IsFalse(Authenticator.IsAuthenticated);
            Authenticator.AuthenticateWithFilePath();
            Assert.IsTrue(Authenticator.IsAuthenticated);
        }
    }
}
