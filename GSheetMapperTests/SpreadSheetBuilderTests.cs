using GSheetMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class SpreadSheetBuilderTests
    {
        [TestMethod]
        public void GetSpreadSheetTest()
        {

            var spreadSheet = new SpreadSheetBuilder()
                .AuthenticateWithFile()
                .SetSpreadSheetId("1ZS_Rw32TpW4Xh91gIoQYFwXpiWOGjAfT2-aYp8aCFEA")
                .Build();

            Assert.IsNotNull(spreadSheet);
        }
    }
}
